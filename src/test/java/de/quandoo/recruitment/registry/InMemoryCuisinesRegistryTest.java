package de.quandoo.recruitment.registry;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import de.quandoo.recruitment.registry.service.CuisinesService;

public class InMemoryCuisinesRegistryTest {
	
	@SuppressWarnings("unchecked")
	@Test(expected = IllegalArgumentException.class)
	public void testInvalidCuisine() {

		CuisinesService cuisinesService = mock(CuisinesService.class);

		when(cuisinesService.cuisineCustomers(null)).thenThrow(IllegalArgumentException.class);

		cuisinesService.cuisineCustomers(null);
	}

	@Test
	public void testCuisineCustomers() {

		CuisinesService cuisinesService = mock(CuisinesService.class);

		List<Customer> customersList = new ArrayList<>();
		customersList.add(new Customer("1"));
		customersList.add(new Customer("2"));
		customersList.add(new Customer("3"));

		when(cuisinesService.cuisineCustomers((Cuisine) notNull())).thenReturn(customersList);

		List<Customer> italianCustomers = cuisinesService.cuisineCustomers(new Cuisine("Italian Cuisine"));

		assertEquals(3, italianCustomers.size());
		assertEquals("1", italianCustomers.get(0).getUuid());
		assertEquals("2", italianCustomers.get(1).getUuid());
		assertEquals("3", italianCustomers.get(2).getUuid());
	}

	@Test
	public void testCustomerCuisines() {

		CuisinesService cuisinesService = mock(CuisinesService.class);

		List<Cuisine> cuisinesList = new ArrayList<>();
		cuisinesList.add(new Cuisine("Italian Cuisine"));
		cuisinesList.add(new Cuisine("French Cuisine"));

		when(cuisinesService.customerCuisines((Customer) notNull())).thenReturn(cuisinesList);

		List<Cuisine> customerCuisines = cuisinesService.customerCuisines(new Customer("1"));
		assertEquals(2, customerCuisines.size());
	}

	@Test
	public void testTopCuisines() {

		CuisinesService cuisinesService = mock(CuisinesService.class);

		List<Cuisine> cuisinesList = new ArrayList<>();
		cuisinesList.add(new Cuisine("Italian Cuisine"));
		cuisinesList.add(new Cuisine("French Cuisine"));

		when(cuisinesService.topCuisines(any(Integer.class))).thenReturn(cuisinesList);

		List<Cuisine> topCuisines = cuisinesService.topCuisines(2);

		assertEquals(2, topCuisines.size());
		assertEquals("Italian Cuisine", topCuisines.get(0).getName());

	}

}