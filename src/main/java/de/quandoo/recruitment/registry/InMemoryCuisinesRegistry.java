package de.quandoo.recruitment.registry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import redis.clients.jedis.Jedis;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

	private static CuisinesRegistry instance;

	private final Jedis jedis;
	
	private Set<String> uniqueCuisines = new HashSet<>();

	private static final String CUSTOMER_KEY = "customer:%s";
	private static final String CUISINE_KEY = "cuisine:%s";
	private static final String TOP_CUISINE_KEY = "topcuisine:%s";

	private InMemoryCuisinesRegistry() {
		jedis = new Jedis("localhost", 6379);
	}

	public static CuisinesRegistry getInstance() {

		if (instance == null) {
			instance = new InMemoryCuisinesRegistry();
		}

		return instance;
	}

	@Override
	public void register(final Customer userId, final Cuisine cuisine) {

		jedis.sadd(String.format(CUSTOMER_KEY, userId.getUuid()), cuisine.getName());
		jedis.sadd(String.format(CUISINE_KEY, cuisine.getName()), userId.getUuid());

		jedis.incr(String.format(TOP_CUISINE_KEY, cuisine.getName()));

		uniqueCuisines.add(cuisine.getName());

	}

	@Override
	public List<Customer> cuisineCustomers(final Cuisine cuisine) {

		List<Customer> customers = new ArrayList<>();

		Set<String> customersPerCuisine = jedis.smembers(String.format(CUISINE_KEY, cuisine.getName()));

		customersPerCuisine.forEach(c -> customers.add(new Customer(c)));

		return customers;
	}

	@Override
	public List<Cuisine> customerCuisines(final Customer customer) {

		List<Cuisine> cuisineList = new ArrayList<>();

		Set<String> cuisinePerCustomer = jedis.smembers(String.format(CUSTOMER_KEY, customer.getUuid()));

		cuisinePerCustomer.forEach(c -> cuisineList.add(new Cuisine(c)));

		return cuisineList;
	}

	@Override
	public List<Cuisine> topCuisines(final int n) {

		List<Cuisine> topCuisines = new ArrayList<>();

		Map<String, Integer> cuisinePerFollowers = new HashMap<>();

		for (String cuisineName : uniqueCuisines) {
			String quantityOfCustomers = jedis.get(String.format(TOP_CUISINE_KEY, cuisineName));
			cuisinePerFollowers.put(cuisineName, Integer.parseInt(quantityOfCustomers));
		}

		cuisinePerFollowers.keySet().forEach(cuisine -> {
			jedis.zadd("top", cuisinePerFollowers.get(cuisine), cuisine);
		});

		Set<String> zrange = jedis.zrevrange("top", 0, n);

		zrange.forEach(c -> topCuisines.add(new Cuisine(c)));

		return topCuisines;
	}
}
