package de.quandoo.recruitment.registry.service;

import java.util.List;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

public class CuisinesService {

	private final CuisinesRegistry registry;

	public CuisinesService(CuisinesRegistry registry) {
		super();
		this.registry = registry;
	}

	public void register(Customer customer, Cuisine cuisine){
		this.registry.register(customer, cuisine);
	}

	public List<Cuisine> customerCuisines(Customer customer){
		return this.registry.customerCuisines(customer);
	}

	public List<Cuisine> topCuisines(int n){
		return this.registry.topCuisines(n);
	}

	public List<Customer> cuisineCustomers(Cuisine cuisine){
		return this.registry.cuisineCustomers(cuisine);
	}
}
